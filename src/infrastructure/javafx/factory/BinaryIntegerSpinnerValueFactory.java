package infrastructure.javafx.factory;

import javafx.scene.control.SpinnerValueFactory;

public class BinaryIntegerSpinnerValueFactory extends SpinnerValueFactory.IntegerSpinnerValueFactory {
    public BinaryIntegerSpinnerValueFactory(int min, int max) {
        super(min, max);
    }

    public BinaryIntegerSpinnerValueFactory(int min, int max, int startValue) {
        super(min, max, startValue);
    }

    @Override
    public void decrement(int var1) {
        if(this.getValue()/2 >= this.getMin()){
            this.setValue(this.getValue()/2);
        }
    }

    @Override
    public void increment(int var1) {
        if(this.getValue()*2 <= getMax()){
            this.setValue(this.getValue()*2);
        }
    }
}
