package infrastructure.javafx.repository;

import domain.entity.Room;
import domain.exceptions.NotFoundException;
import domain.valueobject.Id;
import java.util.ArrayList;

public class RoomRepository implements domain.repository.RoomRepository {


    private RoomRepository instance;

    private ArrayList<Room> saves = new ArrayList<>();

    @Override
    public Room findOneById(Id id) throws NotFoundException {
        for (Room room : saves
                ) {

            if (room.id().toString() == id.toString()) {
                return room;
            }

        }
        throw new NotFoundException("Can't find any room with id: "+id.toString());
    }

    @Override
    public void save(Room room) {
        if(!saves.contains(room)){
            saves.add(room);
        }
    }

    public void delete(Room room){
            saves.remove(room);
    }
}
