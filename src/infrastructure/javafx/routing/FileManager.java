package infrastructure.javafx.routing;

import infrastructure.javafx.routing.exception.NoViewFilesFound;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

public class FileManager {

    private ArrayList<File> files = new ArrayList<>();
    public static final String directory = "infrastructure/javafx/resource";

    private FileManager() throws NoViewFilesFound {
        File[] files = FileManager.getResourceFolderFiles(FileManager.directory);
        if(files == null){
            throw new NoViewFilesFound("No view Files were found in: "+ FileManager.directory);
        }
        for (File file : files ) {
            if (file.isFile()) {
                this.files.add(file);
            }
        }
    }

    public ArrayList<File> generateFileList() {
        return (ArrayList<File>) files.clone();
    }

    public static FileManager create() throws NoViewFilesFound {
        return new FileManager();
    }

    public static String generateLoaderPath(File file){
        return "/"+FileManager.directory+"/"+file.getName();
    }
    private static File[] getResourceFolderFiles (String folder) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource(folder);
        String path = null;
        if (url != null) {
            path = url.getPath();
            File file = new File(path);
            return file.listFiles();
        }
        return null;
    }
}
