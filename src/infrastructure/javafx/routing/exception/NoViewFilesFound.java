package infrastructure.javafx.routing.exception;

public class NoViewFilesFound extends Exception {

    public NoViewFilesFound(String message) {
        super(message);
    }
}
