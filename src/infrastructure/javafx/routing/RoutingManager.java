package infrastructure.javafx.routing;

import infrastructure.javafx.routing.exception.NoViewFilesFound;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RoutingManager {

    private Map<String,Scene> paneList = new HashMap<>();
    private static RoutingManager instance;
    private Stage stage;

    private RoutingManager(Stage primaryStage) {
        this.stage = primaryStage;
        generateControllerList();
    }

    public static RoutingManager create(Stage primaryStage) {
        if (RoutingManager.instance == null) {
            RoutingManager.instance = new RoutingManager(primaryStage);
        }
        return RoutingManager.instance;
    }

    public static RoutingManager get(){
        return RoutingManager.instance;
    }

    private void generateControllerList() {
        try {
            for (File item : FileManager.create().generateFileList()) {
                try {
                    if(this.isFXMLFile(item)){
                        Pane pane = FXMLLoader.load(getClass().getResource(FileManager.generateLoaderPath(item)));
                        this.paneList.put(this.getRouteName(item),new Scene(pane));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (NoViewFilesFound noViewFilesFound) {
            noViewFilesFound.printStackTrace();
        }
    }

    private String getRouteName(File item) {
        int extension = item.getName().lastIndexOf(".");
        return item.getName().substring(0,extension);
    }


    public void route(String route) {
        Scene scene= (Scene)this.paneList.get(route);
        this.stage.setScene(scene);
    }



    private boolean isFXMLFile(File file){
        return file.getName().matches("(.*)\\.fxml");
    }
}
