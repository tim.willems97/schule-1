package infrastructure.javafx.controller;

import infrastructure.javafx.routing.RoutingManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.NoRouteToHostException;

public abstract class AbstractController implements Initializable {

    @FXML
    protected void route(String route){
        RoutingManager.get().route(route);
    }
}
