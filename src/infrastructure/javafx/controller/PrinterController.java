package infrastructure.javafx.controller;

import domain.entity.Printer;
import domain.exceptions.InvalidCapacityException;
import domain.factory.PrinterFactory;
import domain.valueobject.PaperFormat;
import domain.valueobject.RepairStatus;
import domain.valueobject.PrinterTechnology;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class PrinterController extends AbstractController {

    //<editor-fold desc="FXML">
    @FXML
    public Label lbl_id;

    @FXML
    public TextField tf_serialNumber;

    @FXML
    public TextField tf_modell;

    @FXML
    public TextField tf_manufacturer;

    @FXML
    public ChoiceBox<RepairStatus.Status> choice_status;

    @FXML
    public Spinner<Integer> sp_warranty;

    @FXML
    public DatePicker dp_arrivalDate;

    @FXML
    public CheckBox check_colorAble;

    @FXML
    public ChoiceBox<PrinterTechnology.Technology> choice_technologie;

    @FXML
    public ChoiceBox<PaperFormat.Format> choice_papersize;

    @FXML
    public Label lbl_capacity;

    @FXML
    public Label lbl_printed;

    @FXML
    public Label lbl_printedSinceReload;

    @FXML
    public ListView<Printer> list_printer;

    @FXML
    public Button btn_revoke;
    //</editor-fold>

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.choice_papersize.getItems().setAll(PaperFormat.Format.values());
        this.choice_status.getItems().setAll(RepairStatus.Status.values());
        this.choice_technologie.getItems().setAll(PrinterTechnology.Technology.values());
        this.lbl_printed.setText("");
        this.lbl_printedSinceReload.setText("");
        this.lbl_capacity.setText("");
        this.sp_warranty.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100));
        btn_revoke.setOnAction(e -> route("dashboard"));
        initalizeHardware();
    }

    private void initalizeHardware() {
        try {
            this.list_printer.getItems().add(PrinterFactory.create(
                    "1-2-freddy-kommt-vorbei",
                    "ProJet",
                    "HP",
                    RepairStatus.Status.OK,
                    12,
                    LocalDate.now(),
                    PrinterTechnology.Technology.INKJET,
                    true,
                    2000,
                    2000,
                    PaperFormat.Format.A3
            ));
            this.list_printer.getItems().add(PrinterFactory.create(
                    "3-4-er-steht-vor-deiner-tür",
                    "ProJet",
                    "HP",
                    RepairStatus.Status.REPAIR,
                    24,
                    LocalDate.now(),
                    PrinterTechnology.Technology.LASER,
                    true,
                    2000,
                    2000,
                    PaperFormat.Format.A3
            ));
            this.list_printer.getItems().add(PrinterFactory.create(
                    "5-6-jetzt-holt-dich-die-alte-hex",
                    "coolerDrucker",
                    "canon",
                    RepairStatus.Status.OUT_OF_ORDER,
                    3,
                    LocalDate.now(),
                    PrinterTechnology.Technology.INKJET,
                    true,
                    2000,
                    2000,
                    PaperFormat.Format.A4
            ));
        } catch (InvalidCapacityException e) {
            e.printStackTrace();
            //TODO: Remove this method. Its useless
        }
    }

    public void saveAction(ActionEvent actionEvent) {
        Printer printer;
        if (this.list_printer.getSelectionModel().getSelectedItem() != null) {
           /* printer = this.list_printer.getSelectionModel().getSelectedItem();
            try{
                printer = edit(printer);
            }catch (InvalidCapacityException e){
                Alert exceptionAlert = new Alert(Alert.AlertType.ERROR,e.getMessage());
            }*/
            // TODO: make printer editable

        } else {
            try {
                printer = create();
                this.list_printer.getItems().add(printer);
            } catch (InvalidCapacityException e) {
                Alert exceptionAlert = new Alert(Alert.AlertType.ERROR, e.getMessage());
            }
        }
    }

    private Printer create() throws InvalidCapacityException {

        return PrinterFactory.create(
                this.tf_serialNumber.getText().toString(),
                this.tf_modell.getText().toString(),
                this.tf_manufacturer.getText().toString(),
                this.choice_status.getValue(),
                this.sp_warranty.getValue(),
                this.dp_arrivalDate.getValue(),
                this.choice_technologie.getValue(),
                this.check_colorAble.isSelected(),
                200, 200, this.choice_papersize.getValue()
        );
    }

    private Printer edit(Printer printer) throws InvalidCapacityException {
        return PrinterFactory.create(
                this.tf_serialNumber.getText().toString(),
                this.tf_modell.getText().toString(),
                this.tf_manufacturer.getText().toString(),
                this.choice_status.getValue(),
                this.sp_warranty.getValue(),
                this.dp_arrivalDate.getValue(),
                this.choice_technologie.getValue(),
                this.check_colorAble.isSelected(),
                200, 200, this.choice_papersize.getValue()
        );
    }

    public void revokeAction(ActionEvent actionEvent) {

    }
}
