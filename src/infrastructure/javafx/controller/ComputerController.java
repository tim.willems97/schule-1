package infrastructure.javafx.controller;

import domain.entity.Computer;
import domain.exceptions.InvalidCapacityException;
import domain.factory.ComputerFactory;
import domain.factory.PrinterFactory;
import domain.valueobject.ComputerType;
import domain.valueobject.PaperFormat;
import domain.valueobject.PrinterTechnology;
import domain.valueobject.RepairStatus;
import infrastructure.javafx.factory.BinaryIntegerSpinnerValueFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ComputerController extends AbstractController {

    //<editor-fold desc="FXML">
    @FXML
    public Label lbl_id;

    @FXML
    public TextField tf_serialNumber;

    @FXML
    public TextField tf_modell;

    @FXML
    public TextField tf_manufacturer;

    @FXML
    public ChoiceBox<RepairStatus.Status> choice_status;

    @FXML
    public Spinner<Integer> sp_warranty;

    @FXML
    public DatePicker dp_arrivalDate;


    @FXML
    public Button btn_revoke;

    @FXML
    public ChoiceBox<ComputerType.Type> choice_pc_type;

    @FXML
    public Spinner<Integer> spn_ram;

    @FXML
    public TextField tf_cpu;

    @FXML
    public TextField tf_graphic;

    @FXML
    public Spinner<Integer> spn_hdd;

    @FXML
    public Spinner<Integer> spn_ssd;

    @FXML
    public TextField tf_os;
    public ListView<Computer> list_computer;
    //</editor-fold>

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.choice_pc_type.getItems().setAll(ComputerType.Type.values());
        this.choice_status.getItems().setAll(RepairStatus.Status.values());
        this.sp_warranty.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100));
        this.spn_ram.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 4, 1));
        this.spn_hdd.setValueFactory(new BinaryIntegerSpinnerValueFactory(2, 12000,128));
        this.spn_ssd.setValueFactory(new BinaryIntegerSpinnerValueFactory(2, 12000, 128));
        btn_revoke.setOnAction(e -> route("dashboard"));
        initalizeHardware();
    }

    private void initalizeHardware() {
        try {
            this.list_computer.getItems().add(ComputerFactory.create(
                    "1-2-freddy-kommt-vorbei",
                    "Pavillion",
                    "HP",
                    RepairStatus.Status.OK,
                    12,
                    LocalDate.now(),
                    "Intel I7 irgendwas",
                    3,
                    "Windows",
                    ComputerType.Type.OFFICE,
                    "Nvedia irgendwas",
                    2,
                    2
            ));
            this.list_computer.getItems().add(ComputerFactory.create(
                    "3-4 er steht an deiner Tür",
                    "Notebook",
                    "Asus",
                    RepairStatus.Status.OUT_OF_ORDER,
                    12,
                    LocalDate.now(),
                    "Intel I7 irgendwas",
                    3,
                    "Windows",
                    ComputerType.Type.OFFICE,
                    "Nvedia irgendwas",
                    2,
                    2
            ));
            this.list_computer.getItems().add(ComputerFactory.create(
                    "5-6 jetzt-kommt-die-alte-hex",
                    "Think Pad",
                    "Lenovo",
                    RepairStatus.Status.REPAIR,
                    12,
                    LocalDate.now(),
                    "Intel I7 irgendwas",
                    3,
                    "Windows",
                    ComputerType.Type.OFFICE,
                    "Nvedia irgendwas",
                    2,
                    2
            ));
        } catch (InvalidCapacityException e) {
            e.printStackTrace();
            //TODO: Remove this method. Its useless
        }
    }

    public void saveAction(ActionEvent actionEvent) {
        Computer computer;
        if (this.list_computer.getSelectionModel().getSelectedItem() != null) {
            // TODO: make computer editable

        } else {
            try {
                computer = create();
                this.list_computer.getItems().add(computer);
            } catch (InvalidCapacityException e) {
                Alert exceptionAlert = new Alert(Alert.AlertType.ERROR, e.getMessage());
                exceptionAlert.showAndWait();
            }
        }
    }

    private Computer create() throws InvalidCapacityException {

        return ComputerFactory.create(
                this.tf_serialNumber.getText().toString(),
                this.tf_modell.getText().toString(),
                this.tf_manufacturer.getText().toString(),
                this.choice_status.getValue(),
                this.sp_warranty.getValue(),
                this.dp_arrivalDate.getValue(),
                this.tf_cpu.getText().toString(),
                this.spn_ram.getValue(),
                this.tf_os.getText().toString(),
                this.choice_pc_type.getValue(),
                this.tf_graphic.getText().toString(),
                this.spn_hdd.getValue(),
                this.spn_ssd.getValue()
        );
    }

    private Computer edit(Computer printer) throws InvalidCapacityException {
        return ComputerFactory.create(
                this.tf_serialNumber.getText().toString(),
                this.tf_modell.getText().toString(),
                this.tf_manufacturer.getText().toString(),
                this.choice_status.getValue(),
                this.sp_warranty.getValue(),
                this.dp_arrivalDate.getValue(),
                this.tf_cpu.getText().toString(),
                this.spn_ram.getValue(),
                this.tf_os.getText().toString(),
                this.choice_pc_type.getValue(),
                this.tf_graphic.getText().toString(),
                this.spn_hdd.getValue(),
                this.spn_ssd.getValue()
        );
    }

    public void revokeAction(ActionEvent actionEvent) {

    }
}
