package infrastructure.javafx.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class DashboardController extends AbstractController {

    @FXML
    public Button btn_printer;

    @FXML
    public Button btn_pc;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btn_printer.setOnAction(e -> route("printer"));
        btn_pc.setOnAction(e -> route("computer"));
    }
}
