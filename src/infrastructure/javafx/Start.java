package infrastructure.javafx;

import infrastructure.javafx.routing.RoutingManager;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

public class Start extends Application {
    private Stage primaryStage = null;

    public static void main(String[] args) {
        launch(args);
    }



    @Override
    public void start(Stage primaryStage) throws IOException {

        RoutingManager router = RoutingManager.create(primaryStage);
        router.route("dashboard");
        primaryStage.setTitle("Hello World");
        primaryStage.show();
    }


}
