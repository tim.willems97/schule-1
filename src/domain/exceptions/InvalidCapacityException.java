package domain.exceptions;

public class InvalidCapacityException extends Exception {
    public InvalidCapacityException(String s) {
        super(s);
    }
}
