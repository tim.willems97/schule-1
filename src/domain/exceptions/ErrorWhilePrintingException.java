package domain.exceptions;

public class ErrorWhilePrintingException extends Exception {
    public ErrorWhilePrintingException(String message) {
        super(message);
    }
}
