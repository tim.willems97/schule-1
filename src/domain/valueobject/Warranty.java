package domain.valueobject;

import java.time.LocalDate;

public class Warranty {

    /**
     * Warranty in Months
     */
    private int value;

    public Warranty(int warranty) {
        this.value = warranty;
    }

    public LocalDate calculateWarrantyEnd(LocalDate arrivalDate){
        return arrivalDate.plusMonths(this.value);
    }

    @Override
    public String toString() {
        return Integer.toString(this.value);
    }

    public int toInt() {
        return this.value;
    }
}
