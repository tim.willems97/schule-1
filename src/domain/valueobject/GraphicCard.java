package domain.valueobject;

public class GraphicCard {
    private String value;

    public GraphicCard(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }
}
