package domain.valueobject;

public class OS {
    private String value;

    public OS(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }
}
