package domain.valueobject;

import domain.exceptions.InvalidCapacityException;
import domain.exceptions.NotEnoughCapacityException;

public class Capacity {

    private int capacity;
    private int rest;
    private int min_val = 0;

    public Capacity(int capacity, int actual) throws InvalidCapacityException {
        validate(capacity, this.min_val, actual);
        this.capacity = capacity;
        this.rest = actual;
    }

    public Capacity(int capacity, int actual, int min_val) throws InvalidCapacityException {
        validate(capacity, min_val, actual);
        this.capacity = capacity;
        this.rest = actual;
        this.min_val = min_val;
    }

    public void change(int capacity) throws InvalidCapacityException {
        if (capacity < this.min_val) {
            throw new InvalidCapacityException("The Capacity" + Integer.toString(capacity) + " is lower than" + Integer.toString(min_val));
        }
        this.capacity = capacity;
        this.rest = capacity;
    }

    private static void validate(int max_val, int min_val, int actual) throws InvalidCapacityException {
        if (actual > max_val) {
            throw new InvalidCapacityException("The Capacity" + Integer.toString(actual) + " is higher than" + Integer.toString(max_val));
        }
        if (actual < min_val) {
            throw new InvalidCapacityException("The Capacity" + Integer.toString(actual) + " is lower than" + Integer.toString(min_val));
        }
    }

    public void refill() {
        this.rest = this.capacity;
    }

    public void print() throws NotEnoughCapacityException {
        int rest = this.rest;
        rest--;
        if (rest < min_val) {
           throw new NotEnoughCapacityException("Empty");
        }
        this.rest = rest;
    }

    public int toInt() {
        return this.min_val;
    }
    @Override
    public String toString() {
        return Integer.toString(this.capacity);
    }
}
