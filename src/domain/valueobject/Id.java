package domain.valueobject;

public final class Id {

    private int value;

    /**
     * Construct an Id
     * @param id an Integer Value as ID
     */
    public Id(int id){
       this.value = id;
    }

    /**
     * @return returns the ID as Integer
     */
    public int toInt(){
        return this.value;
    }

    /**
     *
     * @return returns the ID as string
     */
    @Override
    public String toString(){
        return Integer.toString(this.value);
    }
}
