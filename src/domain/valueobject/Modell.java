package domain.valueobject;

public final class Modell {

    private String value;

    /**
     * @param modell the label of the Modell
     */
    public Modell(String modell){
        this.value = modell;
    }

    /**
     * @return the Modell as String
     */
    @Override
    public String toString() {
        return this.value;
    }
}
