package domain.valueobject;

public class HardDriveHDD {
    private int value;

    public HardDriveHDD(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return Integer.toString(this.value);
    }
}
