package domain.valueobject;

public class RAM {
    private int value;

    public RAM(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return Integer.toString(this.value);
    }
}
