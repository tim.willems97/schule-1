package domain.valueobject;

public class PrinterTechnology {

    private Technology technology;
    private boolean colorAble;

    public PrinterTechnology(Technology technology, boolean colorAble) {
        this.technology = technology;

    }

    public PrinterTechnology(Technology technology) {
        this.technology = technology;
        this.colorAble = false;
    }

    @Override
    public String toString() {
        return this.technology.toString();
    }

    public Boolean isColorAble() {
        return this.colorAble;
    }

    public enum Technology {
        INKJET("inkjet"), LASER("laser");
        private String value;

        Technology(String status) {
            this.value = status;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }
}
