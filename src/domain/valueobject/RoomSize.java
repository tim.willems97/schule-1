package domain.valueobject;

public class RoomSize {
    private float value;

    public RoomSize(float serialnumber) {
        this.value = serialnumber;
    }

    @Override
    public String toString() {
        return Float.toString(this.value);
    }
}
