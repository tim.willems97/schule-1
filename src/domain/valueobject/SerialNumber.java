package domain.valueobject;

public final class SerialNumber {
    private String value;

    public SerialNumber(String serialnumber){
        this.value = serialnumber;
    }

    @Override
    public String toString(){
        return this.value;
    }
}
