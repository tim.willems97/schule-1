package domain.valueobject;

public class RoomType {
    private RoomType.Type value;

    public RoomType(RoomType.Type value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }

    public enum Type {
        CLASSROOM("Class Room"),
        IT("IT Room"),
        ET("ET Room"),
        CHLabor("CH Labor"),
        SERVICE("Service Room"),
        TEACHER("Teacher "),
        OFFICE("Office"),
        MISC("Miscellaneous");
        private String value;

        Type(String status) {
            this.value = status;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }
}
