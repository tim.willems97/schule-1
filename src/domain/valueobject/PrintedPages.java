package domain.valueobject;

public class PrintedPages {

    private int value;

    public PrintedPages(int pages) {
        this.value = pages;
    }

    public PrintedPages(){
        this.value = 0;
    }

    public void print() {
        this.value++;
    }

    @Override
    public String toString() {
        return Integer.toString(this.value);
    }

    public int toInt() {
        return this.value;
    }

    public void print(int pages) {
        this.value += pages;
    }
}
