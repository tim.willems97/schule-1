package domain.valueobject;

public class HardDriveSSD {
    private int value;

    public HardDriveSSD(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return Integer.toString(this.value);
    }
}
