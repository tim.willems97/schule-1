package domain.valueobject;

public class ComputerType {

    private Type value;

    public ComputerType(Type value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }

    public enum Type {
        GAMING("Gaming"), MULTIMEDIA("Multimedia"), OFFICE("Office");
        private String value;

        Type(String status) {
            this.value = status;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }
}
