package domain.valueobject;

public final class RepairStatus {
    private Status value;

    /**
     * @param status
     */
    public RepairStatus(Status status) {
        this.value = status;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }

    public enum Status {
        OK("ok"), REPAIR("in_repair"), OUT_OF_ORDER("out_of_order");
        private String value;

        Status(String status) {
            this.value = status;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }
}

