package domain.valueobject;

public class CPU {
    private String value;

    public CPU(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }
}
