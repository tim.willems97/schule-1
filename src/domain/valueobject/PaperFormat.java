package domain.valueobject;

public class PaperFormat {

    private Format value;

    public PaperFormat(Format value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }

    public enum Format {
        A3("A3"), A4("A4");
        private String value;

        Format(String status) {
            this.value = status;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }
}
