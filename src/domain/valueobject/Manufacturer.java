package domain.valueobject;

public final class Manufacturer {

    private String value;

    /**
     * @param manufacturer the label of the Manufacturer
     */
    public Manufacturer(String manufacturer){
        this.value = manufacturer;
    }

    /**
     * @return the label of the Manufacturer
     */
    @Override
    public String toString() {
        return this.value;
    }
}
