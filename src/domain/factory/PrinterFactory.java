package domain.factory;

import domain.entity.Printer;
import domain.entity.Room;
import domain.exceptions.InvalidCapacityException;
import domain.valueobject.*;

import java.time.LocalDate;

final public class PrinterFactory {

    public static Printer create(
            String serialnumber,
            String modell,
            String manufacturer,
            RepairStatus.Status printerStatus,
            int warrantyMonths,
            LocalDate deliverDate,
            PrinterTechnology.Technology technology,
            boolean canColor,
            int capacity,
            int restCapacity,
            PaperFormat.Format format,
            Room room


    ) throws InvalidCapacityException {
        return new Printer(
                new SerialNumber(serialnumber),
                new Modell(modell),
                new Manufacturer(manufacturer),
                new RepairStatus(printerStatus),
                new Warranty(warrantyMonths),
                deliverDate,
                room,
                new PrinterTechnology(technology,canColor),
                new PrintedPages(),
                new Capacity(capacity, restCapacity),
                new PaperFormat(format)
                );
    }
}
