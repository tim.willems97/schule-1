package domain.factory;

import domain.entity.Room;
import domain.exceptions.InvalidCapacityException;
import domain.valueobject.*;

import java.time.LocalDate;

final public class ComputerFactory {

    public static Computer create(
            String serialnumber,
            String modell,
            String manufacturer,
            RepairStatus.Status printerStatus,
            int warrantyMonths,
            LocalDate deliverDate,
            String cpu,
            int ram,
            String os,
            ComputerType.Type type,
            String graphicCard,
            int hdd,
            int ssd,
            Room room

    ) throws InvalidCapacityException {
        return new Computer(
                new SerialNumber(serialnumber),
                new Modell(modell),
                new Manufacturer(manufacturer),
                new RepairStatus(printerStatus),
                new Warranty(warrantyMonths),
                deliverDate,
                room,
                new CPU(cpu),
                new RAM(ram),
                new OS(os),
                new ComputerType(type),
                new GraphicCard(graphicCard),
                new HardDriveHDD(hdd),
                new HardDriveSSD(ssd)
        );
    }
}
