package domain.entity;

import domain.valueobject.*;

import java.time.LocalDate;

final public class Computer extends Hardware {

    private static int count = 0;
    private CPU cpu;
    private RAM ram;
    private OS os;
    private ComputerType type;
    private GraphicCard graphicCard;
    private HardDriveHDD hdd;
    private HardDriveSSD ssd;

    public Computer(SerialNumber serialnumber, Modell modell, Manufacturer manufacturer, RepairStatus repairStatus, Warranty warranty, LocalDate arrivalDate,Room room, CPU cpu, RAM ram, OS os, ComputerType type, GraphicCard graphicCard, HardDriveHDD hdd, HardDriveSSD ssd) {

        super(new Id(count++),serialnumber, modell, manufacturer, repairStatus, warranty, arrivalDate, room);
        this.cpu = cpu;
        this.ram = ram;
        this.os = os;
        this.type = type;
        this.graphicCard = graphicCard;
        this.hdd = hdd;
        this.ssd = ssd;
    }

    @Override
    public String toString() {
        return "Computer{" +
                " id=" + id +
                ", serialnumber=" + serialnumber +
                ", modell=" + modell +
                ", manufacturer=" + manufacturer +
                ", repairStatus=" + repairStatus +
                ", warranty=" + warranty +
                ", arrivalDate=" + arrivalDate +
                ", cpu=" + cpu +
                ", ram=" + ram +
                ", os=" + os +
                ", type=" + type +
                ", graphicCard=" + graphicCard +
                ", hdd=" + hdd +
                ", ssd=" + ssd +
                '}';
    }
}
