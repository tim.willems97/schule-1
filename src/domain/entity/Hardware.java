package domain.entity;

import domain.valueobject.*;

import java.time.LocalDate;

public abstract class Hardware {
    protected Id id;
    protected SerialNumber serialnumber;
    protected Modell modell;
    protected Manufacturer manufacturer;
    protected RepairStatus repairStatus;
    protected Warranty warranty;
    protected LocalDate arrivalDate;
    protected Room room;

    public Hardware(Id id, SerialNumber serialnumber, Modell modell, Manufacturer manufacturer, RepairStatus repairStatus, Warranty warranty, LocalDate arrivalDate, Room room) {
        this.id = id;
        this.serialnumber = serialnumber;
        this.modell = modell;
        this.manufacturer = manufacturer;
        this.repairStatus = repairStatus;
        this.warranty = warranty;
        this.arrivalDate = arrivalDate;
        this.room = room;
    }

    public LocalDate calculateWarrantyEnd(){
        return this.warranty.calculateWarrantyEnd(this.arrivalDate);
    }
    public LocalDate getArrivalDate(){
        return this.arrivalDate;
    }

    public abstract String toString();
}
