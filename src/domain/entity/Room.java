package domain.entity;

import domain.valueobject.Id;
import domain.valueobject.RoomType;
import domain.valueobject.RoomSize;

final public class Computer {

    private Id id;

    private RoomType type;
    private RoomSize size;

    public Room (Id id, RoomType type, RoomSize size){
        this.id = id;
        this.type = type;
        this.size = size;
    }

    public Id id(){
        return  this.id;
    }
}
