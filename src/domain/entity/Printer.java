package domain.entity;

import domain.exceptions.ErrorWhilePrintingException;
import domain.exceptions.InvalidCapacityException;
import domain.exceptions.NotEnoughCapacityException;
import domain.valueobject.*;

import java.time.LocalDate;

public final class Printer extends Hardware {
    private static int count = 0;

    private PrinterTechnology technology;
    private PrintedPages printed;
    private Capacity capacity;
    private PaperFormat paperFormat;

    public Printer(SerialNumber serialnumber, Modell modell, Manufacturer manufacturer, RepairStatus repairStatus, Warranty warranty, LocalDate arrivalDate, Room room, PrinterTechnology technology, PrintedPages printed, Capacity capacity, PaperFormat format) {
        super(new Id(count++), serialnumber, modell, manufacturer, repairStatus, warranty, arrivalDate, room);
        this.technology = technology;
        this.printed = printed;
        this.capacity = capacity;
        this.paperFormat = format;
    }

    @Override
    public String toString() {
        return "Printer{" +
                "id=" + this.id +
                ", serialnumber=" + this.serialnumber +
                ", modell=" + this.modell +
                ", manufacturer=" + this.manufacturer +
                ", repairStatus=" + this.repairStatus +
                ", warranty=" + this.warranty +
                ", arrivalDate=" + this.arrivalDate +
                ", technology=" + this.technology +
                ", printed=" + this.printed +
                ", capacity=" + this.capacity +
                ", paperFormat=" + this.paperFormat +
                '}';
    }

    public void refill(int capacity) throws InvalidCapacityException {
        this.capacity.change(capacity);
    }

    public void refill() {
        this.capacity.refill();
    }


    public void print(int pages) throws ErrorWhilePrintingException {
        if (pages <= 0) {
            throw new ErrorWhilePrintingException("Error While Printing: You have to print at least 1 Page");
        }
        int i = pages;
        do {
            i--;
            try {
                this.capacity.print();
            } catch (NotEnoughCapacityException e) {
                throw new ErrorWhilePrintingException("Es konnten nur " + i + " Seiten erfolgreich gedruckt werden. Bitte wechseln Sie das Betriebsmittel!");
            }
            this.printed.print();

        } while (i >= this.capacity.toInt());
    }
}
