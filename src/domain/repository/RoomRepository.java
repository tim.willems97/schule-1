package domain.repository;

import domain.entity.Room;
import domain.exceptions.NotFoundException;
import domain.valueobject.Id;

public interface RoomRepository {
    public Room findOneById(Id id) throws NotFoundException;
    public void save(Room room);
    public void delete(Room room);

}
